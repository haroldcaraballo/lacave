# LACAVE

Propuesta para https://www.mabisy.com para optar por el puesto de maquetador.

Detalles:

El trabajo es para maquetar tiendas online de la plataforma Mabisy.com. 

Requerimos de un profesional de la maquetación web que desarrolle el siguiente trabajo:
Adaptación de diseños personalizados basados en nuestras plantillas.  Es decir, partiendo de una plantilla html base, se deberá adaptar el aspecto para que quede igual que el diseño que nosotros facilitaremos, solo tocando estilos CSS.
Cada maquetación será remunerada con 20 Dólares.
Se tendrán que entregar entre 2 y 3 maquetaciones a la semana.  (Nosotros tardamos entre 6 o 7 horas por maquetación, depende de los conocimientos en maquetación) Las primeras seguro que se tarda un poco más.
El horario será libre, pero cumpliendo con las fechas de entrega establecidas y la comunicación se realizará por los canales internos de la empresa vía Internet
Si se demuestra responsabilidad y conocimientos hay posibilidad de realizar más trabajos en un futuro
Para poder optar al puesto de trabajo, hay que hacer una prueba que consiste en:
Partiendo de esta imagen: https://www.dropbox.com/s/8swno81w8itgh5b/LACAVE_CCAA_190712_ESCRITORIO.jpg?dl=0 
Hay que maquetar en html la zona que quieras de este diseño. Lo que queremos saber es como maquetas el orden del código, etc..
Enviar el código creado por email en un ZIP
Fecha máxima de entrega: 22/10/2019
Quedamos a la espera de la recepción de la prueba para pasar al siguiente paso de la selección, una entrevista personal por Skype.
